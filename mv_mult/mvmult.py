import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule
import time

mod = SourceModule(open("mvmult.cu").read())

mat_vec_mult = mod.get_function("mv_mnozenje")

a = np.ones((2000,2000), dtype = np.int32) #matrica
b = np.ones(2000, dtype = np.int32) #vektor

result_gpu = np.empty(2000, dtype = np.int32) #vektor

startGPU = time.time()
mat_vec_mult(drv.Out(result_gpu), drv.In(a), drv.In(b), block=(200,5,1), grid=(2,1))
endGPU = time.time()

print("GPU: ", result_gpu, endGPU-startGPU)

startCPU = time.time()
result_cpu = np.dot(a, b)
endCPU = time.time()

print("CPU: ", result_cpu, endCPU - startCPU)
