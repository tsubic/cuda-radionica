__global__ void mv_mnozenje(int* result, int* mat, int* vec)
{
  const int idx =blockIdx.x * blockDim.x * blockDim.y + threadIdx.y * blockDim.x + threadIdx.x;

  float sum_product = 0;
  for (int i = 0; i < 2000; i++)
    {
      sum_product += mat[idx * 2000 + i] * vec[i];
    }

  result[idx] = sum_product;
}

