__global__ void zbroji_matrice(float* a, float* b, float* result) //dodavanje parametara
{
  // dvodimenzionalni index niti u bloku
	int i  = blockIdx.x * blockDim.x * blockDim.y + threadIdx.y * blockDim.x + threadIdx.x;
  result[i] = a[i] + b[i]; //zbrajanje elemenata
}
