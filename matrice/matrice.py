import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule

mod = SourceModule(open("matrice.cu").read())

zbroji_matrice = mod.get_function("zbroji_matrice")

n = 1000
a = np.ones((n, n), dtype=np.float32)
b = np.ones((n, n), dtype=np.float32) * 3

result_gpu = np.empty_like(a)

zbroji_matrice(drv.In(a), drv.In(b), drv.Out(result_gpu), block=(500,2,1), grid=(1000,1)) #dodati parametre
#CPU kod

print(result_gpu)

print("CPU rezultat\n", a + b)
