import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule

mod = SourceModule(open("vektori.cu").read())

zbroj_vektora = mod.get_function("zbroj_vektora")

#definiranje vektora
a = np.arange(0,1000,1, dtype=np.int32)
b = np.ones(1000, dtype=np.int32) * 3

resultGPU = np.zeros_like(a)

#pokretanje kernela i definiranje broja niti

zbroj_vektora(drv.In(a), drv.In(b), drv.Out(resultGPU), block=(100,10,1), grid=(1,1))

print(resultGPU)

#CPU kod
#resultCPU = a+b
#print(resultCPU)
