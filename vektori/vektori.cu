
__global__ void zbroj_vektora (int *a, int* b, int* result)//parametri
{
  //index niti
  	const int idx = threadIdx.y * blockDim.x + threadIdx.x;

	result[idx] = a[idx] * b[idx];
  //zbrajanje elemenata
}
