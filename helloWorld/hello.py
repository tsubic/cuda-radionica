# pyCUDA import
import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule

#određivanje datoteke s GPU kernelom
mod = SourceModule(open("hello.cu").read())

#povezivanje kernela s našom funkcijom
hello = mod.get_function("hello")

#pozivanje kernela na GPU
hello(block=(32,1,1), grid=(1,1))

#CPU kod
print("Pozdrav s CPU-a!")
