import pycuda.autoinit
import pycuda.driver as drv
import pycuda.gpuarray as ga
import pycuda.curandom as cm
import pycuda.cumath 
import numpy as np

#nekvalitetnim slučajni brojevi
n_random_choices = 10000000
hits = 0
throws = 0

for i in range (0, n_random_choices):
    throws += 1
    x = np.random.random()
    y = np.random.random()
    dist = np.math.sqrt(x * x + y * y)
    if dist <= 1.0:
        hits += 1

pi = 4 * (hits / throws)
error = abs(pi - np.math.pi)
print("pi is approximately %.16f, error is approximately %.16f" % (pi, error))

#random generatori
x = ga.GPUArray(n_random_choices, dtype=np.float64)
y = ga.GPUArray(n_random_choices, dtype=np.float64)
generator = cm.XORWOWRandomNumberGenerator()
generator.fill_uniform(x)
generator.fill_uniform(y)

result = pycuda.cumath.floor(pycuda.cumath.sqrt(x*x + y*y))
hits_gpu =  n_random_choices - np.sum(result.get())

pi_gpu = 4 * (hits_gpu / n_random_choices)

#aproksimiranje broja Pi
print("pi is approximately %.16f, error is approximately" % pi_gpu)
