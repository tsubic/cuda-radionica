import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule
#modul gpuarray
import pycuda.gpuarray as ga
# modul cumath
import pycuda.cumath as cm

# definiranje matrica

a = np.ones((20,20), dtype = np.float32)
b = np.ones((20,20), dtype = np.float32)
#result_gpu = np.zeros((20,20), dtype=np.float32)

a_gpu = ga.to_gpu(a)
b_gpu = ga.to_gpu(b)

#zbroj
#result_gpu = a_gpu + b_gpu

result_gpu = cm.sin(a_gpu)

print(result_gpu.get())
#rad s cumath funkcijama
